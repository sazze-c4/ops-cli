"use strict";

const bigtree = require('big-tree-cli');
const Table = require('cli-table2');
const _ = require('lodash');
const Netmask = require('netmask').Netmask;

class Addr {

  static table(addrs) {
    let colNames = ['IP Address', 'Mask', 'Network', 'Zone ID', 'VLAN', 'Assigned'];
    let table = new Table({
      head: colNames,
      colWidths: [18, 18, 24, 10, 8, 10]
    });

    for (let addr of addrs) {
      let network = new Netmask(addr.ipAddress + '/' + addr.mask);

      table.push([
        addr.ipAddress,
        addr.mask,
        network.base + '/' + network.bitmask,
        addr.zoneId,
        addr.vlan,
        (addr.available ? 'NO' : 'YES')
      ]);
    }

    table.push([
      {colSpan: colNames.length, hAlign: 'right', content: `Number of Results: ${addrs.length}`}
    ]);

    return table.toString();
  }

  static summary(addr) {
    let network = new Netmask(addr.ipAddress + '/' + addr.mask);

    return `
ip:        ${addr.ipAddress}
mask:      ${addr.mask}
network:   ${network.base}/${network.bitmask}
zoneId:    ${addr.zoneId}
vlan:      ${addr.vlan}
assigned:  ${(addr.available ? 'NO' : 'YES')}
`
  }

  static ipOnly(addr) {
    return addr.ipAddress;
  }

}

module.exports = Addr;