"use strict";

const _ = require('lodash');
const read = require('read');
const bind = require('co-bind');

class Util {

  static uc(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  static strPad(str, padChar, width, padMode) {
    padMode = _.isUndefined(padMode) ? 1 : padMode;
    let repeatNum = width - str.length;
    repeatNum = repeatNum >= 0 ? repeatNum : 0;
    let pad = padChar.repeat(repeatNum);
    return padMode >= 0 ? str + pad : pad + str;
  }

  static cobind(that, gen) {
    return co(bind(gen, that));
  }

  static readString(title, def, password) {
    return new Promise(function(resolve, reject) {
      read({
        prompt: title + ': ',
        silent: password ? true : false,
        default: def
      }, function(err, result) {
        if (err) {
          command.log.error(err);
        }
        resolve(result);
      });
    });
  }

  static readPipe() {
    return new Promise(function(resolve, reject) {
      let output = new Buffer([]);
      process.stdin.on('error', reject);
      process.stdin.on('data', function(data) {
        output = Buffer.concat([output, data]);
      });
      process.stdin.on('end', function() {
        resolve(output.toString('utf8'));
      });
    });
  }

  static toLines(str) {
    return _.filter(str.trim().split(/\r?\n/g));
  }

  static formatTableRow(row, colWidths, mode) {
    return _.map(row, function(col, index) {
      col = '' + (col || '--');
      return Util.strPad(col, ' ', colWidths[index] || 14, mode);
    }).join(' ');
  }

}

module.exports = Util;