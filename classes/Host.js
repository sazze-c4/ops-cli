"use strict";

const bigtree = require('big-tree-cli');
const Table = require('cli-table2');
const _ = require('lodash');
const os = require('os');
const numeral = require('numeral');

const nameIndex = {
  type: {
    1: 'BARE_METAL',
    2: 'VM',
    3: 'APPLIANCE'
  },
  provisionState: {
    0: 'UNPROVISIONED',
    1: 'PROVISIONING',
    2: 'PROVISIONED',
    3: 'REPROVISION',
    4: 'DEPROVISIONING',
    5: 'DEPROVISIONED'
  },
  provisioningStep: {
    0: 'COBBLER_CONFIG',
    1: 'CHEF_CONFIG',
    2: 'BOOT',
    3: 'KICKSTART',
    4: 'CHEF'
  },
  deprovisioningStep: {
    0: 'CHEF_CONFIG',
    1: 'COBBLER_CONFIG',
    2: 'SALT_CONFIG',
    3: 'BOOT'
  }
};

class Host {

  static provisioningStep(host) {
    return host.provisionState > 3 ? nameIndex.deprovisioningStep[host.provisioningStep] : nameIndex.provisioningStep[host.provisioningStep];
  }

  static provisionState(host) {
    return nameIndex.provisionState[host.provisionState];
  }

  static type(host) {
    return nameIndex.type[host.type];
  }

  static table(hosts, pageNum) {
    let colNames = ['ID', 'FQDN', 'IP Address', 'Active', '# CPU', 'RAM', 'Pool', 'Zone', 'Chef Role', 'Prov. State', 'Prov. Step'];
    let table = new Table({
      head: colNames,
      colWidths: [8, 48, 20, 8, 8, 8, 8, 16, 48, 20, 20]
    });

    for (let host of hosts) {
      table.push([
        host.hostId,
        host.fqdn,
        host.primaryIp ? host.primaryIp.ipAddress : '',
        host.active ? 'YES' : 'NO',
        host.cpu,
        host.ram,
        host.pool ? host.pool.name : '',
        host.zone ? host.zone.name : '',
        host.chefRole,
        this.provisionState(host),
        this.provisioningStep(host)
      ]);
    }

    table.push([
      {colSpan: colNames.length / 2, hAlign: 'center', content: `Number of Results: ${hosts.length}`},
      {colSpan: colNames.length / 2, hAlign: 'center', content: `Page Number: ${pageNum}`},
    ]);

    return table.toString();
  }

  static summary(host) {
    return `
fqdn:      ${host.fqdn}
chefRole:  ${host.chefRole}
ip:        ${host.primaryIp ? host.primaryIp.ipAddress : '--'}
vlan:      ${host.primaryIp ? host.primaryIp.vlan : '--'}
pool:      ${host.pool ? host.pool.name : '--'}
zone:      ${host.zone ? host.zone.name : '--'}
kvmHost:   ${host.kvmHost ? host.kvmHost.fqdn : '--'}
ipmiHost:  ${host.ipmiHost ? host.ipmiHost.fqdn : '--'}
provState: ${this.provisionState(host)}
provStep:  ${this.provisioningStep(host)}
macs:      ${host.macs ? host.macs.map(function (mac) { return mac.hwAddress; }) : '--'}
active:    ${host.active ? 'YES' : 'NO'}
total:     ${host.cpu || '--'} x ${host.ram || '--'}M
allocated: ${host.allocatedCpu || '--'} x ${host.allocatedRam || '--'}M
`
  }

  static tree(host, args) {
    let self = this;
    args = args || {};
    try {

      return bigtree(hostNode(host)).toString();

    } catch (e) {
      return e.stack;
    }

    function hostNode(host) {
      let rootNodes = [];
      pushRoot('hostId', host.hostId);
      pushRoot('fqdn', host.fqdn);
      pushRoot('provisionState', self.provisionState(host));
      pushRoot('provisioningStep', self.provisioningStep(host));
      pushRoot('active', host.active ? 'YES' : 'NO');
      pushRoot('cpu', host.allocCpu);
      pushRoot('ram', host.allocRam);
      pushRoot('isVMHost', host.vmHost ? 'YES' : 'NO');
      pushRoot('chefRole', host.chefRole);
      pushRoot('domain', host.domain.domain);
      pushRoot('pool', host.pool.name);
      pushRoot('zone', host.zone.name);
      if (host.primaryIp) {
        pushRoot('primaryIp', host.primaryIp.ipAddress);
      }
      if (args.macs) {
        pushRoot('macs', host.macs.map(function(mac) {
          return node('hwAddress', mac.hwAddress)
        }));
      }
      if (args.disks) {
        pushRoot('disks', host.disks.map(function(disk) {
          return node('disk' + disk.order, [
            node('diskId', disk.diskId),
            node('size', disk.size),
            node('order', disk.order),
            node('partitions', disk.partitions.map(partitionNode))
          ])
        }));
      }
      if (args.raids) {
        pushRoot('raids', host.raid.map(function(raid) {
          return node('/dev/' + raid.device, [
            node('raidId', raid.raidId),
            node('mount', raid.mount),
            node('level', raid.level),
            node('partitions', raid.partitions.map(partitionNode))
          ])
        }));
      }
      if (args.interfaces) {
        pushRoot('netInterfaces', host.netInterfaces.map(function(iface) {
          return node(iface.name, [
            node('hwAddress', iface.mac ? iface.mac.hwAddress : ''),
            node('ipv4', iface.ipAddressV4.map(ipNode))
          ])
        }));
      }
      if (args.bridges) {
        pushRoot('netInterfaceBridges', host.netInterfaceBridges.map(function(iface) {
          return node(iface.name, [
            node('ipAddress', [
              ipNode(iface.ipAddress)
            ]),
            node('interface', iface.interface.interfaces.map(ifaceBondNode))
          ])
        }));
      }
      if (args.bonds) {
        pushRoot('netInterfaceBonds', host.netInterfaceBonds.map(ifaceBondNode));
      }

      if (host.ipmiHost) {
        pushRoot('ipmiHost', [hostNode(host.ipmiHost)]);
      }

      if (host.vmHost && host.guests) {
        pushRoot('guests', host.guests.map(hostNode));

      } else if (!host.vmHost && host.kvmHost) {
        pushRoot('kvmHost', [hostNode(host.kvmHost)]);
      }

      return node(host.name, rootNodes);

      function pushRoot(name, value) {
        rootNodes.push(node(name, value));
      }
    }


    function node(name, value) {
      return {
        label: name,
        nodes: _.isArray(value) ? value : ['' + value]
      }
    }

    function ipNode(ip) {
      return node(ip.ipAddress, [
        node('vlan', ip.vlan),
        node('mask', ip.mask),
        node('zoneId', ip.zoneId)
      ])
    }

    function ifaceBondNode(iface) {
      let nodes = [];
      if (iface.mac) {
        nodes.push(node('hwAddress', iface.mac.hwAddress));
      }
      nodes.push(node('ipv4', iface.ipAddressV4.map(ipNode)));
      return node(iface.name, nodes)
    }

    function partitionNode(part) {
      return node(part.mount, [
        node('partId', part.partitionId || part.diskPartitionId),
        node('fsType', part.fsType),
        node('size', part.size + 'M'),
        node('primary', part.primary ? true : false),
        node('order', part.order)
      ])
    }
  }

  /**
   *
   * @param hosts
   * @param opts
   *    bmOnly - show bare metals only
   *    vmOnly - show vms only
   *    allNames - show all names
   */
  static rackMap(mapResult, opts) {

    let output = '';

    let minCol = mapResult.minCol;
    let maxCol = mapResult.maxCol;
    let numCols = maxCol - minCol + 1;
    const colWidth = 49;

    for (let domain in mapResult.domains) {
      let domainMap = mapResult.domains[domain];
      output += os.EOL + [ domain, '-'.repeat(domain.length) ].join(os.EOL) + os.EOL;

      let rowData = calculateRowData(domainMap);

      let table = new Table({
        rowHeights: _.map(new Array(rowData.numRows), function() { return rowData.rowHeight; }),
        colWidths: _.map(new Array(numCols), function() { return colWidth; })
      });

      for (let rowName in domainMap) {
        let rowMap = domainMap[rowName];
        let _row = new Array(numCols);

        for (let colName in rowMap) {
          let colData = rowMap[colName];
          let allocPerCell = allocatedPerCell(colData);
          let num4x4 = calculateNum4x4(colData);

          let title = rowName + colName + ` (${colData.bareMetalHosts.length + colData.vmHosts.length} hosts; ${colData.vmHosts.length} vms; ${allocPerCell.allocCpu}/${allocPerCell.totalCpu} x ${numeral(allocPerCell.allocRam).format('0,0')}/${numeral(allocPerCell.totalRam).format('0,0')}M)`;

          let subWidth = colWidth - 8;
          let cpuLength = parseInt(Math.min(allocPerCell.totalCpu, allocPerCell.allocCpu) / allocPerCell.totalCpu * subWidth);
          let ramLength = parseInt(Math.min(allocPerCell.totalRam, allocPerCell.allocRam) / allocPerCell.totalRam * subWidth);

          let cpuTitle = '|'.repeat(cpuLength) + '-'.repeat(subWidth - cpuLength) + ' CPU';
          let ramTitle = '|'.repeat(ramLength) + '-'.repeat(subWidth - ramLength) + ' RAM';

          let cellText = [
            title,
            cpuTitle,
            ramTitle,
            `${num4x4.alloc}/${num4x4.total} x [ 4 x 4096M ]`
          ];

          if (opts.byHost) {
            for (let kvmHost in colData.treeHosts) {
              let vms = colData.treeHosts[kvmHost];
              let host = mapResult.hosts[kvmHost];
              cellText = cellText.concat([
                kvmHost.replace(/\..*/, '') + ` (${host.allocatedCpu}/${host.cpu} x ${host.allocatedRam}/${host.ram}M)`
              ].concat(indentArray(vms)))
            }

          } else if (opts.byZone) {
            let zones = filterByZones(colData);
            for (let zoneName in zones) {
              cellText.push(zoneName + ` (${zones[zoneName].length} hosts)`);
              cellText = cellText.concat(indentArray(zones[zoneName]));
            }

          } else {
            cellText = cellText.concat(colData.bareMetalHosts).concat(colData.vmHosts);
          }
          _row[parseInt(colName) - minCol] = cellText.map(function(fqdn) { return fqdn.replace(/\..*/, ''); }).join(os.EOL)
        }

        table.push(_row);
      }

      output += table.toString() + os.EOL;

    }

    return output;

    function calculateNum4x4(colData) {
      let calc = {
        alloc: 0,
        total: 0
      };

      for (let fqdn in colData.treeHosts) {
        let host = mapResult.hosts[fqdn];

        let totalNum4x4 = 0;
        let allocNum4x4 = 0;
        for (let ram = host.ram, cpu = host.cpu; cpu >= 0 && ram >= 0; cpu -= 4, ram -= 4096, totalNum4x4++);
        for (let ram = host.allocatedRam, cpu = host.allocatedCpu; cpu >= 0 && ram >= 0; cpu -= 4, ram -= 4096, allocNum4x4++);

        calc[fqdn] = {
          alloc: allocNum4x4,
          total: totalNum4x4
        };

        calc.alloc += allocNum4x4;
        calc.total += totalNum4x4;

      }

      return calc;
    }

    function allocatedPerCell(colData) {
      let alloc = {
        totalRam: 0,
        totalCpu: 0,
        allocRam: 0,
        allocCpu: 0
      };

      for (let fqdn in colData.treeHosts) {
        let host = mapResult.hosts[fqdn];
        if (!host) {
          continue;
        }

        alloc.totalRam += host.ram;
        alloc.totalCpu += host.cpu;

        alloc.allocRam += host.allocatedRam;
        alloc.allocCpu += host.allocatedCpu;
      }

      return alloc;
    }

    function indentArray(arr) {
      return arr.map(function (vm) {
        return '  ' + vm.replace(/\..*/, '') + ` (${mapResult.hosts[vm].cpu} x ${mapResult.hosts[vm].ram}M)`;
      });
    }

    function calculateRowData(domain) {
      let numRows = Object.keys(domain);
      let rowHeight = 0;
      for (let rowName in domain) {
        let row = domain[rowName];
        for (let colName in row) {
          let colData = row[colName];
          let height = colData.bareMetalHosts.length + colData.vmHosts.length;
          if (opts.byZone) {
            height += Object.keys(filterByZones(colData)).length;

          } else {
            height += 2;
          }
          rowHeight = Math.max(height + 2, rowHeight);
        }
      }

      return {
        numRows: numRows,
        rowHeight: rowHeight
      }
    }

    function filterByZones(colData) {
      let zones = {};
      for (let name of [].concat(colData.bareMetalHosts).concat(colData.vmHosts)) {
        let host = mapResult.hosts[name];
        if (!zones[host.zone.name]) {
          zones[host.zone.name] = [];
        }
        zones[host.zone.name].push(host.fqdn);
      }

      return zones;
    }

    //opts = opts || {};
    //let hostByDomain = {};
    //let minCol = 100000;
    //let maxCol = 0;
    //let maxNumHosts = 0;
    //for (let host of hosts) {
    //  if (opts.bmOnly && 1 != host.type) {
    //    continue;
    //  }
    //  if (opts.vmOnly && 2 != host.type) {
    //    continue;
    //  }
    //  if (host.pool && /^[A-Z]\d$/.test(host.pool.name) && host.zone) {
    //    let pool = host.pool;
    //    let zone = host.zone;
    //    let matches = /^([A-Z]+)(\d+)$/.exec(pool.name);
    //    if (matches[1] && matches[2]) {
    //      let row = matches[1];
    //      let col = matches[2];
    //
    //      if (!hostByDomain[zone.domain]) {
    //        hostByDomain[zone.domain] = {};
    //      }
    //      if (!hostByDomain[zone.domain][row]) {
    //        hostByDomain[zone.domain][row] = {};
    //      }
    //      if (!hostByDomain[zone.domain][row][col]) {
    //        hostByDomain[zone.domain][row][col] = [];
    //      }
    //
    //      hostByDomain[zone.domain][row][col].push(host);
    //      minCol = Math.min(parseInt(col), minCol);
    //      maxCol = Math.max(parseInt(col), maxCol);
    //      maxNumHosts = Math.max(hostByDomain[zone.domain][row][col].length, maxNumHosts);
    //    }
    //  }
    //}
    //
    //if (maxCol < minCol) {
    //  throw new Error('No hosts found matching these filters')
    //}
    //
    //let numCols = maxCol - minCol + 1;
    //
    //let numRows = Object.keys(hostByDomain).length;
    //
    //let output = '';
    //
    //_.each(hostByDomain, function(hostByPool, domainName) {
    //
    //  output += domainName + os.EOL + '-'.repeat(domainName.length) + os.EOL;
    //
    //  let table = new Table({
    //    colWidths: _.map(new Array(numCols), function() { return 32; }),
    //    rowHeights: _.map(new Array(numRows), function () { return (opts.allNames ? maxNumHosts : 1) + 2; })
    //  });
    //
    //  _.each(hostByPool, function(row, rowName) {
    //    let _row = new Array(numCols);
    //    _.each(row, function(cell, colName) {
    //      let names;
    //      if (opts.allNames) {
    //        names = cell.map(function(host) { return host.name; })
    //      } else {
    //        names = [ cell.length + ' hosts' ];
    //      }
    //      _row[parseInt(colName) - minCol] = [rowName + colName, '----'].concat(names).join(os.EOL);
    //    });
    //    table.push(_row);
    //  });
    //
    //  output += table.toString();
    //});
    //
    //return output;
  }

}

module.exports = Host;