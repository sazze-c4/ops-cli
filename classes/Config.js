"use strict";

const path = require('path');
const _ = require('lodash');
const fs = require('fs');

const CONFIG_FILE_NAME = '.ops.conf';

const defaultConfig = {
  f5: {
    host: '127.0.0.1',
    proto: 'https',
    port: 443,
    user: '',
    pass: '',
    strict: false,
    debug: false
  }
};

function loadConfig() {
  let configFile = readConfigFile();

  return _.merge({}, defaultConfig, configFile);
}

function readConfigFile() {
  let dirs = _.unique(['.', process.env.HOME || process.env.USERPROFILE || '.']);

  let configFile = null;

  for (let i = 0; i < dirs.length; i++) {
    if (fs.existsSync(path.join(path.resolve(dirs[i]), CONFIG_FILE_NAME))) {
      configFile = path.join(path.resolve(dirs[i]), CONFIG_FILE_NAME);
      break;
    }
  }

  if (!configFile) {
    return {};
  }

  return JSON.parse(fs.readFileSync(configFile));
}

module.exports = loadConfig();