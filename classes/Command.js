"use strict";

const OpsClient = require('@sazze/ops-client');
const Logger = require('./Logger');
const User = require('./User');
const Util = require('./Util');
const Path = require('path');
const EventEmitter = require('events').EventEmitter;
const bind = require('co-bind');

class Command extends EventEmitter {

  constructor(args, controllerDir) {
    super();
    global.command = this;
    if (args._.length > 2) {
      this.arg = args._.splice(2 - args._.length);
      if (1 == this.arg.length) {
        this.arg = this.arg[0];
      }
    }

    this.name = args._.pop();
    this.controller = args._.pop();

    this.args = args;
    this.log = new Logger();
    this.user = User.load();

    this.dir = controllerDir;

    if (!this.dir) {
      this.dir = Path.join(__dirname, '..', 'commands');
    }
  }

  execute() {
    let execTime = process.hrtime();
    try {
      let controller = this.controller ? Util.uc(this.controller) : 'index';
      controller = Path.normalize(Path.join(this.dir, controller));
      let task = require(controller);

      co(bind(task[this.name], this))
        .then(function(result) {
          if (result) {
            this.log.info(result);
          }
          printExecTime.call(this);
          this.emit('end');
        }.bind(this))
        .catch(onError.bind(this));

    } catch (e) {
      onError.call(this, e);
    }

    function onError(e) {
      try {
        this.log.error(e);
      } catch(e) {
        console.log(e);
      }
      printExecTime.call(this);
      this.emit('end');
    }

    function printExecTime() {
      if (this.args.execTime) {
        execTime = process.hrtime(execTime);
        execTime = (execTime[0] + execTime[1] / 1e9).toFixed(3);
        this.log.info('Execution time: ' + execTime)
      }
    }

  }

}

module.exports = Command;