"use strict";

const bigtree = require('big-tree-cli');
const Table = require('cli-table2');
const _ = require('lodash');
const moment = require('moment');

class Pool {

  static table(pools) {
    let colNames = ['ID', 'Name', 'Zone ID', 'Created'];
    let table = new Table({
      head: colNames,
      colWidths: [10, 24, 10, 28]
    });

    for (let pool of pools) {
      table.push([
        pool.poolId,
        pool.name,
        pool.zoneId,
        moment(pool.createdTime * 1000).format('YYYY-MM-DD HH:mm:ss ZZ')
      ]);
    }

    table.push([
      {colSpan: colNames.length, hAlign: 'right', content: `Number of Results: ${pools.length}`}
    ]);

    return table.toString();
  }

  static summary(pool) {
    return `
id:       ${pool.poolId}
name:     ${pool.name}
zone id:  ${pool.zoneId}
created:  ${moment(pool.createdTime * 1000).format('YYYY-MM-DD HH:mm:ss ZZ')}
`
  }

  static nameOnly(pool) {
    return pool.name;
  }

}

module.exports = Pool;