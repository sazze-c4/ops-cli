"use strict";

const bigtree = require('big-tree-cli');
const Table = require('cli-table2');
const _ = require('lodash');
const Netmask = require('netmask').Netmask;

class Subnet {

  static table(subnets) {
    let colNames = ['Network', 'Mask', 'Zone ID', 'VLAN'];
    let table = new Table({
      head: colNames,
      colWidths: [24, 18, 10, 8]
    });

    for (let subnet of subnets) {
      let network = new Netmask(subnet.network + '/' + subnet.mask);

      table.push([
        subnet.network + '/' + network.bitmask,
        subnet.mask,
        subnet.zoneId,
        subnet.vlan
      ]);
    }

    table.push([
      {colSpan: colNames.length, hAlign: 'right', content: `Number of Results: ${subnets.length}`}
    ]);

    return table.toString();
  }

  static summary(subnet) {
    let network = new Netmask(subnet.network + '/' + subnet.mask);

    return `
network:  ${subnet.network}/${network.bitmask}
mask:     ${subnet.mask}
zoneId:   ${subnet.zoneId}
vlan:     ${subnet.vlan}
`
  }

  static subnetOnly(subnet) {
    let network = new Netmask(subnet.network + '/' + subnet.mask);

    return subnet.network + '/' + network.bitmask;
  }

}

module.exports = Subnet;