"use strict";

const bigtree = require('big-tree-cli');
const Table = require('cli-table2');
const _ = require('lodash');
const moment = require('moment');

class Domain {

  static table(domains) {
    let colNames = ['Domain', 'Created'];
    let table = new Table({
      head: colNames,
      colWidths: [40, 28]
    });

    for (let domain of domains) {
      table.push([
        domain.domain,
        moment(domain.createdTime * 1000).format('YYYY-MM-DD HH:mm:ss ZZ')
      ]);
    }

    table.push([
      {colSpan: colNames.length, hAlign: 'right', content: `Number of Results: ${domains.length}`}
    ]);

    return table.toString();
  }

  static summary(domain) {
    return `
domain:   ${domain.domain}
created:  ${moment(domain.createdTime * 1000).format('YYYY-MM-DD HH:mm:ss ZZ')}
`
  }

  static domainOnly(domain) {
    return domain.domain;
  }

}

module.exports = Domain;