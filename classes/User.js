"use strict";

const _ = require('lodash');
const fs = require('fs');
const read = require('read');
const Path = require('path');
const OpsClient = require('@sazze/ops-client');
const Util = require('./Util');
const OPS_CACHE_FILE = Path.join(process.env.HOME, '.opsrc');

let hasLoaded = false;

class User {

  constructor(data) {
    _.merge(this, {
      version: require('../package').version,
      preferences: {},
    }, data);

    command.on('end', function() {
      this.save();
    }.bind(this));
  }

  get isLoggedIn() {
    return !!this.authToken;
  }

  get client() {
    let baseUrl = command.args.__dev__ ? 'http://172.16.97.141:8000/' : 'http://c4.ops.sazze.com/';
    return new OpsClient(baseUrl, 0, this.authToken);
  }

  pref(key, value) {
    if (value) {
      this.preferences[key] = value;
    } else {
      return this.preferences[key];
    }
  }

  logout() {
    delete this.authToken;
    return this.save();
  }

  login() {
    let self = this;
    return co(function *() {
      let username = yield Util.readString('username');
      let password = yield Util.readString('password', '', true);

      try {
        let result = yield self.client.login(username, password);

        if (result.resp && result.resp.headers && result.resp.headers['x-auth-token']) {
          self.authToken = result.resp.headers['x-auth-token'];
        }

        self.save();

        return result;

      } catch (e) {
        command.log.error(e);
      }
    });
  }

  save() {
    if (hasLoaded) {
      try {
        let text = JSON.stringify(this, null, 2);
        fs.writeFileSync(OPS_CACHE_FILE, text);

      } catch (e) {
        command.log.error(e);
      }
    }
    return this;
  }

  static load() {
    try {
      fs.accessSync(OPS_CACHE_FILE, fs.F_OK);
    } catch (e) {
      fs.writeFileSync(OPS_CACHE_FILE, JSON.stringify({}));
    }

    try {
      let text = fs.readFileSync(OPS_CACHE_FILE).toString('utf8');
      let data = JSON.parse(text);
      hasLoaded = true;
      return new User(data);

    } catch (e) {
      command.log.error(e);
      return new User();
    }
  }
}

module.exports = User;