"use strict";

const _ = require('lodash');
const fs = require('fs');
const OPS_DEBUG_FILE = '/tmp/ops-debug.log';
const Response = require('@sazze/ops-client').Response;

class Logger {

  constructor() {
    this.stream = fs.createWriteStream(OPS_DEBUG_FILE);
    command.on('end', function() {
      this.stream.close();
    }.bind(this));
  }

  debug() {
    this.stream.write(argsToString.apply(this, arguments));
  }

  info() {
    process.stdout.write(argsToString.apply(this, arguments));
  }

  _error() {
    process.stderr.write(argsToString.apply(this, arguments));
  }

  error(e) {
    if (e instanceof Error) {
      this.debug(e.stack);
      this._error(e.message);
    } else if (e instanceof Response) {
      this.debug(e);
      this._error(e.body);
    } else {
      this._error(e);
    }
  }

}

module.exports = Logger;

function argsToString() {
  return Array.prototype.slice
      .call(arguments)
      .map(function(arg) {

        if (arg instanceof Response) {
          arg = arg.body.error == null ? arg.body : arg.body.error;
        }

        if (_.isString(arg)) {
          return arg;
        } else if (command.args.pretty) {
          return JSON.stringify(arg, null, 2);
        }  else {
          return JSON.stringify(arg);
        }
      })
      .join(' ') + '\n';
}