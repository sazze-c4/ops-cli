"use strict";

const Table = require('cli-table2');
const _ = require('lodash');
const moment = require('moment');

class Zone {
  static table(zones) {

    let table = new Table({
      head: ['Zone ID', 'Name', 'Domain', 'Chef Server', 'Created Time'],
      colWidths: [10, 24, 32, 32, 32]
    });

    for (let zone of zones) {
      table.push([
        zone.zoneId,
        zone.name,
        zone.domain,
        zone.chefServer,
        moment(zone.createdTime * 1000).format('YYYY-MM-DD hh:mm:ss A')
      ]);
    }

    return table.toString();
  }

  static summary(zone) {
    return `
zoneId:      ${zone.zoneId}
name:        ${zone.name}
domain:      ${zone.domain}
chefServer:  ${zone.chefServer}
createdTime: ${moment(zone.createdTime * 1000).format('YYYY-MM-DD hh:mm:ss A')}
`.trim();
  }
}

module.exports = Zone;