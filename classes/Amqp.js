"use strict";

const amqp = require('@sazze/amqp');
const _ = require('lodash');

const baseConfig = {
  host: command.user.amqp ? command.user.amqp.host : '127.0.0.1',
  vhost: '/ops',
  user: 'ops',
  password: command.user.amqp ? command.user.amqp.password : ''
};

class Amqp {
  static consumer(config, handler) {
    return new amqp.consumer(_.merge(baseConfig, config), handler);
  }
}

module.exports = Amqp;