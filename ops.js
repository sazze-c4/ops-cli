#!/usr/bin/env node
"use strict";

global.co = require('co');

const config = require('./classes/Config');

const Command = require('./classes/Command');
const args = require('minimist')(process.argv.slice(2));

let cmd = new Command(args);

cmd.execute();