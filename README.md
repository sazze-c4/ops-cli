# ops-cli

C4 Operations Command Line Interface

Official command line interface to [sazze-c4/ops](https://gitlab.com/sazze-c4/ops)

### Install

```bash
npm install -g @sazze/ops-cli
```

### Usage

```bash
ops --help
```

### Config

Configuration can be specified in a JSON file named `.ops.conf` located in the user's home directory.

## Host Commands

### Search (* Coming Soon)

Find hosts that match a perl RegExp pattern.

**Usage:**

```bash
ops host search PATTERN [OPTIONS]
```

* `PATTERN`: the perl RegExp to match against
* `OPTIONS`: (optional) options to pass to the search
    * `-ma`: (optional) match the pattern against specific attributes (can be specified multiple times) (default: `fqdn`, `chefRole`, and `mac`)
    * `-ra`: (optional) the attribute to return if a match is found (default: `fqdn`)

All matches are written to `STDOUT` with 1 match per line.

**Examples:**

1) Only match against a host's `chefRole` attribute

```bash
ops search host "developer" -am chefRole
```

Resposne:

```bash
dev-craig.c1.cs1.sazze.com
dev-ajaso.c1.cs1.sazze.com
...
```

### Filter

Like search, but uses the old c3.ops.sazze.com filter style (nothing fancy allowed here, just a "string contains" check).  Filters against `fqdn`, `chefRole`, and `mac` attributes.

**Usage:**

```bash
ops host filter FILTER_STRING
```

* `FILTER_STRING`: the string to filter hosts with

**Examples:**

1) Find hosts containing "envoy"

```bash
ops host filter "envoy"
```

Response:

```bash
mc-envoy-main1.c1.cs1.sazze.com
mc-envoy-main2.c1.cs1.sazze.com
ipmi-mc-envoy-main1.c1.cs1.sazze.com
ipmi-mc-envoy-main2.c1.cs1.sazze.com
...
```

### Show

Get information about a host

**Usage:**

```bash
ops host show FQDN [OPTIONS]
```

* `FQDN`: the FQDN of the host to get information for
* `OPTIONS`: (optional) options to pass to show
    * `--raw`: (optional) show raw `JSON` host object
        * `--pretty`: (optional) pretty print the raw JSON output
    * `--tree`: (optional) show host info in a tree format
        * `--all`: (optional) expand all host attributes
        * `--macs`: (optional) expand host mac addresses
        * `--disks`: (optional) expand host disks
        * `--raids`: (optional) expand host raids
        * `--interfaces`: (optional) expand host interfaces
        * `--bridges`: (optional) expand host bridges
        * `--bonds`: (optional) expand host bonds
        * `--guests`: (optional) expand host guests
        * `--kvmHost`: (optional) expand host kvm host
        * `--ipmiHost`: (optional) expand host ipmi host

Can get info for multiple hosts by writing hostname(s) to `STDIN` (1 per line)

**Examples:**

1) show host host1.example.com

```bash
ops host show host1.example.com
```

Response:

```bash
fqdn:     host1.example.com
chefRole: my-chef-role
ip:       192.168.1.1
mac:      AA:BB:CC:DD:EE:FF
```

2) show multiple hosts

```bash
ops host filter example.com | ops host show
```

Response:

```bash
fqdn:     host1.example.com
chefRole: my-chef-role
ip:       192.168.1.1
mac:      AA:BB:CC:DD:EE:FE

fqdn:     host2.example.com
chefRole: my-chef-role
ip:       192.168.1.2
mac:      AA:BB:CC:DD:EE:FF
```

### Clone

Clone an existing host. Outputs the json representation of the host.

Most common use case is to pipe the output of `ops host clone` to `ops host create`

Can also direct the output to a file for editing prior to consumption by `ops host create`

**Usage:**

```bash
ops host clone DONOR_FQDN --cloneName FQDN [--mac MAC_ADDR] [--noInterfaces] [--zone ZONE] [--pool POOL] [--vmHostId FQDN|ID] [--chefRole ROLE] [--pretty]
```

* `DONOR_FQDN`: the FQDN of the host to clone
* `--cloneName FQDN`: the `FQDN` for the new host
* `--mac MAC_ADDR`: (optional) the mac address to use for the new host.  `MAC_ADDR` should be of the format `AA:BB:CC:DD:EE:FF`.  This option may be specified more than once.  Mac addresses will be applied to interfaces in the order received.
* `--noInterfaces`: (optional) do not clone the network interfaces
* `--zone ZONE`: (optional) the name of the zone for the new host (default: the donor host's zone)
* `--pool POOL`: (optional) the name of the pool for the new host (default: the donor host's pool)
* `--vmHostId FQDN|ID`: (optional) the `FQDN` or `ID` of the host that will host the new virtual machine. (default: the donor host's vmHostId).  This options is ignored if the donor host is not a virtual machine.
* `--chefRole ROLE`: (optional) the chef role for the new host (default: the donor host's chef role)
* `--pretty`: (optional) pretty print the JSON output (useful for writing to a file for future editing)

**Examples:**

1) Clone a host

```bash
ops host clone host1.example.com --cloneName host2.example.com --pretty
```

Response:

```javascript
{
  "fqdn": "host2.example.com",
  "name": "host2",
  "cpu": 4,
  "ram": 4096,
  "type": 2,
  "active": 1,
  "disks": [
    {
      "size": 34816,
      "order": 0,
      "partitions": [
        {
          "mount": "/boot",
          "fsType": "ext4",
          "size": 200,
          "primary": 1,
          "order": 0
        },
        {
          "mount": "swap",
          "fsType": "swap",
          "size": 2048,
          "primary": 0,
          "order": 0
        },
        {
          "mount": "/",
          "fsType": "ext4",
          "size": 0,
          "primary": 0,
          "order": 0
        }
      ]
    }
  ],
  "vmHost": 0,
  "chefRole": "chef-role",
  "domain": {
    "domain": "example.com"
  },
  "pool": {
    "poolId": 1,
    "name": "example pool",
    "zoneId": 1
  },
  "zone": {
    "zoneId": 1,
    "name": "example zone",
    "domain": "example.com",
    "chefServer": "chef.example.com"
  },
  "raid": [],
  "netInterfaces": [
    {
      "name": "eth0"
    }
  ],
  "netInterfaceBonds": [],
  "vmHostId": 1234,
  "vmDiskPrefixes": []
}

```

### Map

Create a map of where hosts are located.

Accepts host names on the command line or from `STDIN` and maps them to their locations.

**Usage:**

```bash
ops host map [FQDN] [--byHost | --byZone]
```

* `FQDN`: (optional) the `FQDN` of the host to map.  This option can be specified more than once. (default: read host `FQDN`'s from `STDIN`)
* `--byHost`: (optional) organize hosts by virtual machine host
* `--byZone`: (optional) organize hosts by zone

**Examples:**

1) Map a couple of known hosts

```bash
ops host map host1.example.com host2.example.com
```

2) Map hosts returned from a filter

```bash
ops host filter example.com | ops host map --byHost
```

3) Map all hosts

```bash
ops host filter | ops host map --byHost
```

### Create

Create a host from a json object read from STDIN.

Most common use case is to pipe the output of `ops host clone` to `ops host create`

**Usage:**

```bash
ops host create
```

**Examples:**

1) Create host from file

```bash
cat host1.example.com.json | ops host create
```

2) Create host from clone

```bash
ops clone host1.example.com | ops host create
```

### Remove

```bash
ops host rm FQDN
```

### Provision

```bash
ops host provision FQDN
```

### Deprovision

```bash
ops host deprovision FQDN
```

### Create Bridge

Add an interface on a host to a bridge (create the bridge if it doesn't exist).

```bash
ops host mkBr FQDN BRIDGE INTERFACE
```

* `FQDN`: the `FQDN` of the host
* `BRIDGE`: the name of the bridge to add the interface to (i.e. `br0`)
* `INTERFACE`: the name of the interface to add to the bridge

**Examples:**

```bash
ops host mkBr host1.example.com br0 bond0
```

### Remove Bridge

```bash
ops host rmBr FQDN BRIDGE
```

## Address Commands

### List

```bash
ops addr list
```

### Show

```bash
ops addr show
```

### Assign

```bash
ops addr assign
```

### Unassign

```bash
ops addr unassign
```

## License

ISC License (ISC)

Copyright (c) 2016, Sazze, Inc.

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.