"use strict";

const iControl = require('icontrol');
const _ = require('lodash');

const config = require('../classes/Config').f5;

class F5 {
  static *showPool() {
    let bigip = new iControl(config);

    let name = _.isArray(this.arg) ? this.arg[0] : this.arg;

    let url = `/ltm/pool/${name}`;

    if (this.args.members) {
      url += '/members';
    }

    bigip.list(url, function (err, res) {
      if (err) {
        this.log.error(err.stack || err.message || err);
        return;
      }

      this.log.info(res);
    }.bind(this));
  }

  static *addPoolMember() {
    let bigip = new iControl(config);

    let poolName = this.arg[0];
    let name = this.arg[1];
    let addr = this.arg[2];

    let member = {
      name: name,
      address: addr
    };

    let url = `/ltm/pool/${poolName}/members`;

    bigip.create(url, member, function (err, res) {
      if (err) {
        this.log.error(err.stack || err.message || err);
        return;
      }

      this.log.info(res);
    }.bind(this));
  }

  static *rmPoolMember() {
    let bigip = new iControl(config);

    let poolName = this.arg[0];
    let name = this.arg[1];

    let url = `/ltm/pool/${poolName}/members/${name}`;

    bigip.delete(url, function (err, res) {
      if (err) {
        this.log.error(err.stack || err.message || err);
        return;
      }

      this.log.info(res);
    }.bind(this));
  }

  static *showNode() {
    let bigip = new iControl(config);

    let fqdn = _.isArray(this.arg) ? this.arg[0] : this.arg;

    bigip.list(`/ltm/node/${fqdn}`, function (err, res) {
      if (err) {
        this.log.error(err.stack || err.message || err);
        return;
      }

      this.log.info(res);
    }.bind(this));
  }

  static *disableNode() {
    let bigip = new iControl(config);

    let fqdn = _.isArray(this.arg) ? this.arg[0] : this.arg;

    bigip.modify(`/ltm/node/${fqdn}`, {session: 'user-disabled'}, function (err, res) {
      if (err) {
        this.log.error(err.stack || err.message || err);
        return;
      }

      this.log.info(res);
    }.bind(this));
  }

  static *enableNode() {
    let bigip = new iControl(config);

    let fqdn = _.isArray(this.arg) ? this.arg[0] : this.arg;

    bigip.modify(`/ltm/node/${fqdn}`, {session: 'user-enabled'}, function (err, res) {
      if (err) {
        this.log.error(err.stack || err.message || err);
        return;
      }

      this.log.info(res);
    }.bind(this));
  }
}

module.exports = F5;