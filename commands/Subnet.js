"use strict"

const _ = require('lodash');
const cli = require('cli-table2');
const cSubnet = require('../classes/Subnet');
const os = require('os');

class Subnet {
  static *list() {
    let opts = {
      zoneId: 0,
      vlan: 0
    };

    if (this.args.table) {
      opts.full = 1;
    }

    if (this.args.zoneId) {
      opts.zoneId = this.args.zoneId;
    }

    if (this.args.vlan) {
      opts.vlan = this.args.vlan;
    }

    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let result = yield this.user.client.send('get', `/ipv${ipv}/subnets`, opts);

    if (this.args.raw) {
      return result.body.results;
    } else if (this.args.table) {
      return cSubnet.table(result.body.results, opts.page || 1);
    } else if (this.args.i) {
      return result.body.results.map(cSubnet.summary).join('');
    } else {
      return result.body.results.map(cSubnet.subnetOnly).join(os.EOL);
    }
  }

  static *show() {

  }

  static *create() {
    let data = {
      network: (_.isArray(this.arg) ? this.arg[0] : this.arg),
      vlan: this.args.vlan,
      zone: this.args.zone,
      domain: this.args.domain,
      force: !!this.args.force
    };

    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let result = yield this.user.client.send('post', `/ipv${ipv}/createSubnet`, data);

    return result.body.result;
  }

  static *delete() {
    let data = {
      network: (_.isArray(this.arg) ? this.arg[0] : this.arg)
    };

    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let result = yield this.user.client.send('post', `/ipv${ipv}/deleteSubnet`, data);

    return result.body.result;
  }
}

module.exports = Subnet;