"use strict";

const _ = require('lodash');
const moment = require('moment');
const cHost = require('../classes/Host');
const cUtil = require('../classes/Util');
const cAmqp = require('../classes/Amqp');
const pasync = require('pasync');
const Table = require('cli-table2');
const os = require('os');

class Host {

  static *filter() {
    let opts = {
      'Host.includePrimaryIp': 1
    };

    if (this.args.table) {
      opts.full = 1;
    }

    if (this.args.page) {
      opts.page = this.args.page;
    }

    if (this.args.limit) {
      opts.limit = this.args.limit;
    }

    if (this.args.type) {
      opts.type = this.args.type;
    }

    if (this.args.zoneId) {
      opts.zoneId = this.args.zoneId;
    }

    let arg = this.arg;

    if (this.args.e) {
      arg = '';
    }

    let resp = yield this.user.client.find(arg, opts);
    let results = resp.body.results;

    if (this.args.e) {
      let txt = this.arg;
      let firstSlash = txt.indexOf('/');
      let lastSlash = txt.lastIndexOf('/');
      if (0 !== firstSlash || -1 === lastSlash || firstSlash > lastSlash) {
        return `Invalid regex ${txt}`;
      }

      let re = new RegExp(txt.substring(firstSlash + 1, lastSlash), txt.substr(lastSlash + 1));
      results = _.filter(results, function(result) {
        let fqdn = _.isString(result) ? result : result.fqdn;
        return re.test(fqdn);
      });
    }

    if (this.args.raw) {
      return results;
    } else if (this.args.table) {
      return cHost.table(results, opts.page || 1);
    } else {
      return results.join('\n');
    }
  }

  static *show() {
    let self = this;
    let opts = {
      includeGuests: this.args.guests ? 1 : 0,
      includeKvmHost: this.args.kvmHost ? 1 : 0,
      includeIpmiHost: this.args.ipmiHost ? 1 : 0,
      'Host.includePrimaryIp': 1,
      ipAddress: this.args.ipAddress ? 1 : 0
    };

    if (!this.arg) {
      this.arg = cUtil.toLines(yield cUtil.readPipe());
    }

    if (!_.isArray(this.arg)) {
      this.arg = [this.arg];
    }

    yield pasync.mapLimit(this.arg, 20, function(arg) {

      return cUtil.cobind(this, function *() {
        let result = yield this.user.client.host(arg, opts);
        let host = result.body.result;

        if (this.args.raw) {
          this.log.info(host);

        } else if (this.args.tree) {
          if (this.args.all) {
            this.args.macs = true;
            this.args.disks = true;
            this.args.raids = true;
            this.args.interfaces = true;
            this.args.bridges = true;
            this.args.bonds = true;
            this.args.guests = true;
            this.args.kvmHost = true;
            this.args.ipmiHost = true;
          }

          this.log.info(cHost.tree(host, this.args))

        } else {
          this.log.info(cHost.summary(host))
        }

        this.log.info('\n');
      });

    }.bind(this));
  }

  static *map() {
    let hosts = _.isArray(this.arg) ? this.arg : cUtil.toLines(yield cUtil.readPipe());

    this.log.info(`Building rack map for ${hosts.length} hosts...`);
    let resp = yield this.user.client.send('post', '/host/buildRackMap', {fqdns: hosts});
    this.log.info(`Finished building rack map for ${hosts.length} hosts.`);

    if (null != resp.body.error) {
      throw new Error(resp.body.error.message);
    }

    return cHost.rackMap(resp.body.result, this.args);
  }

  static *rm() {
    yield this.user.client.setStatus(this.arg, 0);
  }

  static *activate() {
    yield this.user.client.setStatus(this.arg, 1);
  }

  static *provision() {
    let result = yield this.user.client.provision(this.arg, this.args);

    return null == result.error ? `Started provisioning ${this.arg}` : result.error;
  }

  static *provisionStateReset() {
    let resp = yield this.user.client.send('get', `/host/${this.arg}/resetProvisionState`, this.args);
    return null == resp.error ? resp.body : resp.error;
  }

  static *deprovision() {
    let result = yield this.user.client.deprovision(this.arg, this.args);

    return null == result.error ? `Started deprovisioning ${this.arg}` : result.error;
  }

  static *reprovision() {
    let result = yield this.user.client.reprovision(this.arg, this.args);

    return null == result.error ? `Started reprovisioning ${this.arg}` : result.error;
  }

  static *clone() {
    let args = _.merge({}, this.args, {
      donorName: this.arg,
      noInterfaces: this.args.noInterfaces ? 1 : 0,
      chefRole: this.args.chefRole,
      vmDiskPrefix: this.args.vmDiskPrefix
    });

    let result = yield this.user.client.send('post', '/Host/clone', args);

    if (result.resp.statusCode != 200) {
      result = result.body && result.body.error ? result.body.error : result.body;
    } else {
      result = result.body.result;
    }
    return result;
  }

  static *create() {
    let data = yield cUtil.readPipe();

    try {
      data = JSON.parse(data);
      let result = yield this.user.client.send('post', `/host/${data.fqdn}/create`, data);

      return result.body;

    } catch (e) {
      this.log.error(e);
    }
  }

  static *mkBr() {
    if (!_.isArray(this.arg) || this.arg.length < 3) {
      this.log.error('USAGE: ops host mkBr [fqdn] [bridgeName] [interfaceName]');
      return;
    }
    let fqdn = this.arg[0];
    let bridgeName = this.arg[1];
    let interfaceName = this.arg[2];
    return yield this.user.client.createBridge(fqdn, bridgeName, interfaceName);
  }

  static *rmBr() {
    if (!_.isArray(this.arg) || this.arg.length < 2) {
      this.log.error('USAGE: ops host rmBr [fqdn] [bridgeName]');
      return;
    }
    let fqdn = this.arg[0];
    let bridgeName = this.arg[1];
    return yield this.user.client.deleteBridge(fqdn, bridgeName);
  }

  static *capacity() {
    let self = this;
    //
    //let hosts;
    //if (!this.arg || !this.arg.length) {
    //  hosts = cUtil.toLines(yield cUtil.readPipe());
    //} else {
    //  hosts = [ this.arg ];
    //}

    let resp = yield this.user.client.find('kvm');
    if (resp.error) {
      throw new Error(resp.error);
    }

    let hosts = _.filter(resp.body.results, function(fqdn) { return !/ipmi/.test(fqdn); });

    let numHosts = 0;

    let colNames = ['Host Name', 'CPU Alloc', 'CPU Total', 'RAM Alloc', 'RAM Used', 'RAM Total', 'Disk Used', 'Disk Total', 'Num Hosts'];
    let colWidths = [24, 14, 14, 14, 14, 14, 14, 14];
    let totals = ['TOTAL', 0, 0, 0, 0, 0, 0, 0, 0];

    self.log.info(cUtil.formatTableRow(colNames, colWidths, -1));

    yield pasync.mapLimit(hosts, 20, function(host) {
      return co(function *() {
        try {
          let resp = yield self.user.client.send('get', `/host/${host}/availableCapacity`);
          if (!resp.error) {
            let cap = resp.body.result;
            let est = cap.estimate;
            numHosts += est.numHosts;
            if (self.args.all || est.numHosts > 0) {
              self.log.info(formatRow([host.replace(/\..*/, ''), est.cpuAlloc, est.cpuTotal, est.ramAlloc, est.ramUsed, est.ramTotal, est.diskUsed, est.diskTotal, est.numHosts]));
            }
          }

        } catch (e) {
          self.log.error(e);
        }
      });
    });

    return formatRow(totals);

    function formatRow(row) {
      if (totals !== row) {
        for (let col = 1; col < colNames.length; totals[col] += row[col++] || 0);
      }

      return cUtil.formatTableRow(_.map(row, function(col, index) {
        if (!self.args.raw && /disk|ram/i.test(colNames[index])) {
          if (col > 1024 * 1024 * 1024 ) {
            col = (col / (1024 * 1024 * 1024)).toFixed(3) + 'GB'
          } else if (col > 1024 * 1024) {
            col = (col / (1024 * 1024)).toFixed(3) + 'MB';
          } else if (col > 1024) {
            col = (col / 1024).toFixed(3) + 'KB';
          }
        }
        return col;
      }), colWidths, -1);
    }
  }

  static *setAttribute() {
    let idx = 0;
    return yield this.user.client.send('post', `/host/${this.arg[idx++]}/setAttribute`, {
      table: this.arg[idx++],
      primaryKey: this.arg[idx++],
      column: this.arg[idx++],
      value: this.arg[idx++]
    });
  }

  static *watch() {
    let self = this;

    let regex;
    if (self.args.e) {
      regex = new RegExp(self.args.e);
    }

    let consumer = cAmqp.consumer({
      exchange: {
        name: 'provisionWatch.fanout',
        type: 'fanout'
      },
      queue: {
        options: {
          durable: false,
          exclusive: false,
          autoDelete: true,
        }
      },
      routingKey: 'event.provisionWatch.env.production.application.c4OpsSazzeCom'
    }, function(message, channel, deliveryInfo) {
      try {
        message = JSON.parse(message);
        let fqdn = message.args && message.args.fqdn ? message.args.fqdn : '';

        if (self.arg && self.arg != fqdn) {
          // skip if we don't want to watch it
          return;

        } else if (regex && !regex.test(fqdn)) {
          // skip if we don't match the regex
          return;
        }

        let prefix = `[ ${moment(message.time).format('YYYY-MM-DD HH:mm:ss.SSS')} - ${message.source} ( ${message.hostname.replace(/\..*/, '')} )${fqdn ? ' - ' + fqdn.replace(/\..*/, '') : ''} ]: `;
        let data = (_.isString(message.data) ? message.data : JSON.stringify(message.data, null, 2))
          .split('\n')
          .map(function(line) {
            return prefix + line;
          })
          .join('\n');
        command.log.info(data)

      } catch (e) {
        command.log.error(e);
      }
    });
    consumer.start();
    let cleanup = _.once(function () {
      consumer.stop();
    });
    process.on('SIGINT', cleanup);
    process.on('uncaughtException', cleanup);
    process.on('exit', cleanup);
  }

  static *help() {
    return `
ops host [subcommand] [arg1..n]

Subcommands:

  filter        READ ONLY Search hosts by FQDN, MAC, and Chef Role
    [arg1]        REQUIRED must be a string matching a FQDN, MAC, or Chef Role
    --table       OPTIONAL show a nicely formatted table of results
                           (limited by --page AND --limit for performance reasons)
    --page        OPTIONAL show the specified page of results (default: 1)
    --limit       OPTIONAL specify the number of results in the table (default: 25)
    --type        OPTIONAL limit the results to a specific host type (BM: 1, VM: 2, APPLIANCE: 3)
    --zoneId      OPTIONAL limit the results to a specific zone id
    OUTPUT      List of fqdns (except in the case of --table)
                
  show          READ ONLY Display information for a single host
                  Host info is displayed in several formats: simple (default) OR tree
    [arg1]        OPTIONAL a valid and active fqdn
    [stdin]       OPTIONAL Reads a newline delimited list of fqdns from stdin pipe
                           A common use case for pipe from stdin is with filter
                           i.e. "ops host filter kvm | grep -v ipmi | ops host show"
    Simple / Tree Options:
    --kvmHost     OPTIONAL also lookup/display the kvm hostname
                           (simple: shows hostname, tree: shows more kvm host details ; applies to VM hosts only)
    --ipmiHost    OPTIONAL also lookup/display the ipmi hostname
                           (simple: shows hostname, tree: shows more ipmi host details)
    --tree        OPTIONAL show host details visually as a tree (default: display minimal details)
    Tree Specific Options:
    --macs        OPTIONAL show all MAC addresses as a subtree of the host
    --disks       OPTIONAL show all Disks as a subtree of the host
    --raids       OPTIONAL show all Raids as a subtree of the host
    --interfaces  OPTIONAL show all Hardware Network Interfaces as a subtree of the host
    --bridges     OPTIONAL show all Bridge Network Interfaces as a subtree of the host
    --bonds       OPTIONAL show all Bond Network Interfaces as a subtree of the host
    --guests      OPTIONAL show all Guests on this host as a subtree of the host (applies to KVM hosts only)
    --all         OPTIONAL enable all the above flags
    OUTPUT      Print out the host summary info

  map           READ ONLY Display a visual rack map of all hosts piped from stdin
    [stdin]       REQUIRED reads a newline delimited list of fqdns from stdin pipe
    --byHost      OPTIONAL organize display by KVM host and VM relationships
    --byZone      OPTIONAL organize display by zone relationships
    OUTPUT      Table where each cell represents a Pool with the name format [A-Z][0-9]
                    containing a breakdown of all hosts in the rack

  rm            WRITE ONLY Set host "active" status to 0
    [arg1]        REQUIRED must be a valid FQDN
    OUTPUT      Raw JSON output

  activate      WRITE ONLY Set host "active" status to 1
    [arg1]        REQUIRED must be a valid FQDN
    OUTPUT      Raw JSON output

  clone         READ ONLY Copy a host's configuration with modifications and display the result for review
    [arg1]        REQUIRED donor name as FQDN
    --cloneName   OPTIONAL clone name as FQDN
    --noInterfaces OPTIONAL exclude all interfaces from the clone
    --chefRole    OPTIONAL specify the chef role
    --vmDiskPrefix (vm OPTIONAL) specify the vm disk prefix
    --zone        OPTIONAL specify the zone name
    --pool        OPTIONAL specify the pool name
    --vmHostId    (vm REQUIRED) specify the KVM host for a VM
    --mac         (vm OPTIONAL, bm REQUIRED) comma separated list of unformatted MAC addresses
    OUTPUT      Raw JSON output

  create        WRITE READ Creates a new host from the configuration piped from stdin
    [stdin]       REQUIRED a JSON of the host to be created
    OUTPUT      Raw JSON output

  mkBr          WRITE ONLY Create a bridge for the given host
    [arg1]        REQUIRED fqdn
    [arg2]        REQUIRED bridge name
    [arg3]        REQUIRED interface name
    OUTPUT      Raw JSON output

  rmBr          WRITE ONLY Remove a bridge from the given host
    [arg1]        REQUIRED fqdn
    [arg2]        REQUIRED bridge name
    OUTPUT      Raw JSON output

  capacity      READ ONLY Poll every KVM host for (Total, Allocated, Used) x (CPU, Mem, Disk)
                  and calculate the number of remaining VM hosts that may be allocated
                  for a given spec.
    --ram         OPTIONAL spec amount of RAM (default: 4GB)
    --cpu         OPTIONAL spec number of CPUs (default: 4)
    --disk        OPTIONAL spec amount of Disk (default: 32GB)
    --raw         OPTIONAL display only raw Byte values (where applicable)
    --all         OPTIONAL display all values (including hosts without available capacity)
    OUTPUT      Table with columns
                  "Host Name" KVM host name
                  "CPU Alloc" number of allocated CPUs on the KVM (based on virsh)
                  "CPU Total" total number of CPUs on the KVM (based on /proc/cpuinfo)
                  "RAM Alloc" amount of RAM currently allocated (based on virsh)
                  "RAM Used"  amount of RAM currently in use on the KVM (based on "free")
                  "RAM Total" total amount of RAM on the KVM (based on "free")
                  "Num Hosts" number remaining hosts on the KVM

  provision     WRITE ONLY Start provisioning a host by FQDN
    [arg1]        REQUIRED fqdn to be provisioned (don't forget to check "ops host show" ; this only works for hosts that are DEPROVISIONED or UNPROVISIONED)

  deprovision   WRITE ONLY Deprovision a host by FQDN
    [arg1]        REQUIRED fqdn to be deprovisioned (don't forget to check "ops host show" ; this only works for hosts that are PROVISIONED)
`
  }

}

module.exports = Host;