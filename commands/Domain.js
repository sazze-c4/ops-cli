"use strict"

const _ = require('lodash');
const cli = require('cli-table2');
const cDomain = require('../classes/Domain');
const os = require('os');

class Domain {
  static *list() {
    let opts = {};

    if (this.args.table) {
      opts.full = 1;
    }

    let result = yield this.user.client.send('get', `/domain/list`, opts);

    if (result.error != null) {
      throw result.error;
    }

    if (this.args.raw) {
      return result.body.results;
    } else if (this.args.table) {
      return cDomain.table(result.body.results, opts.page || 1);
    } else if (this.args.i) {
      return result.body.results.map(cDomain.summary).join('');
    } else {
      return result.body.results.map(cDomain.domainOnly).join(os.EOL);
    }
  }

  static *show() {

  }

  static *create() {
    let data = {
      domain: (_.isArray(this.arg) ? this.arg[0] : this.arg)
    };

    let result = yield this.user.client.send('post', `/domain/create`, data);

    if (result.error != null) {
      throw result.error;
    }

    if (this.args.raw) {
      return result.body.result;
    } else if (this.args.i) {
      return cDomain.summary(result.body.result);
    } else {
      return cDomain.domainOnly(result.body.result);
    }
  }

  static *delete() {
    let data = {
      domain: (_.isArray(this.arg) ? this.arg[0] : this.arg)
    };

    let result = yield this.user.client.send('post', `/domain/delete`, data);

    if (result.error != null) {
      throw result.error;
    }

    return '';
  }
}

module.exports = Domain;