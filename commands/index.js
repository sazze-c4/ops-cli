"use strict";

class Index {

  static *index() {

    if (this.args.version) {
      let pkg = require('../package');
      return pkg.name + ' version ' + pkg.version;

    } else {
      return `
USAGE: ops [--version]
`;
    }

  }

  static *whoami() {
    return yield this.user.client.send('get', '/');
  }

  static *login() {
    let result = yield this.user.login();

    return result.result;
  }

  static *logout() {
    this.user.logout();
  }

}


module.exports = Index;