"use strict"

const _ = require('lodash');
const cli = require('cli-table2');
const cPool = require('../classes/Pool');
const os = require('os');

class Pool {
  static *list() {
    let opts = {
      zoneId: 0
    };

    if (this.args.table) {
      opts.full = 1;
    }

    if (this.args.zoneId) {
      opts.zoneId = this.args.zoneId;
    }

    let result = yield this.user.client.send('get', `/pool/list`, opts);

    if (result.error != null) {
      throw result.error;
    }

    if (this.args.raw) {
      return result.body.results;
    } else if (this.args.table) {
      return cPool.table(result.body.results, opts.page || 1);
    } else if (this.args.i) {
      return result.body.results.map(cPool.summary).join('');
    } else {
      return result.body.results.map(cPool.nameOnly).join(os.EOL);
    }
  }

  static *show() {

  }

  static *create() {
    let data = {
      name: (_.isArray(this.arg) ? this.arg[0] : this.arg),
      zoneId: (this.args.zoneId ? this.args.zoneId : 0)
    };

    let result = yield this.user.client.send('post', `/pool/create`, data);

    if (result.error != null) {
      throw result.error;
    }

    if (this.args.raw) {
      return result.body.result;
    } else if (this.args.i) {
      return cPool.summary(result.body.result);
    } else {
      return cPool.nameOnly(result.body.result);
    }
  }

  static *delete() {
    let data = {
      poolId: (_.isArray(this.arg) ? this.arg[0] : this.arg)
    };

    let result = yield this.user.client.send('post', `/pool/delete`, data);

    if (result.error != null) {
      throw result.error;
    }

    return '';
  }
}

module.exports = Pool;