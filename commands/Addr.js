"use strict"

const _ = require('lodash');
const cli = require('cli-table2');
const cAddr = require('../classes/Addr');
const os = require('os');

class Addr {
  static *list() {
    let opts = {
      zoneId: 0,
      vlan: 0,
      available: undefined
    };

    if (this.args.table) {
      opts.full = 1;
    }

    if (this.args.zoneId) {
      opts.zoneId = this.args.zoneId;
    }

    if (this.args.vlan) {
      opts.vlan = this.args.vlan;
    }

    if (this.args.u && !this.args.a) {
      opts.available = 1;
    }

    if (this.args.a && !this.args.u) {
      opts.available = 0;
    }

    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let result = yield this.user.client.send('get', `/ipv${ipv}/getIpAddresses`, opts);

    if (this.args.raw) {
      return result.body.results;
    } else if (this.args.table) {
      return cAddr.table(result.body.results, opts.page || 1);
    } else if (this.args.i) {
      return result.body.results.map(cAddr.summary).join('');
    } else {
      return result.body.results.map(cAddr.ipOnly).join(os.EOL);
    }
  }

  static *show() {
    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let ipAddr = (_.isArray(this.arg) ? this.arg[0] : this.arg);

    let result = yield this.user.client.send('get', `/ipv${ipv}/getIpAddress`, {ipAddress: ipAddr});

    if (this.args.raw) {
      return result.body.result;
    } else if (this.args.table) {
      return cAddr.table([result.body.result], opts.page || 1);
    } else if (this.args.i) {
      return cAddr.summary(result.body.result);
    } else {
      return cAddr.ipOnly(result.body.result);
    }
  }

  static *assign() {
    if (!_.isArray(this.arg) || this.arg.length != 3) {
      //TODO: error
      return;
    }

    let data = {
      ipAddress: this.arg[0],
      fqdn: this.arg[1],
      interfaceName: this.arg[2]
    };

    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let result = yield this.user.client.send('post', `/ipv${ipv}/assignIpAddress`, data);

    //TODO: check for error

    return result.body.result;
  }

  static *unassign() {
    if (!_.isArray(this.arg) || this.arg.length != 2) {
      //TODO: error
      return;
    }

    let data = {
      ipAddress: this.arg[0],
      fqdn: this.arg[1]
    };

    let ipv = 4;

    if (this.args['6']) {
      ipv = 6;
    }

    let result = yield this.user.client.send('post', `/ipv${ipv}/unAssignIpAddress`, data);

    //TODO: check for error

    return result.body.result;
  }
}

module.exports = Addr;