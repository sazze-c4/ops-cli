"use strict";

const os = require('os');
const cZone = require('../classes/Zone');
const cUtil = require('../classes/Util');
const pasync = require('pasync');

class Zone {
  static *list() {
    let opts = {};
    if (this.arg) {
      opts.domain = this.arg;
    }
    let resp = yield this.user.client.listZones(opts);
    if (null != resp.error) {
      throw new Error(resp.body.error.message);
    }
    let results = resp.body.results;
    if (this.args.table) {
      return cZone.table(results);

    } else if (this.args.withIdOnly) {
      return results.map(function(z) {
        return '' + z.zoneId;
      }).join(os.EOL);
    }

    return results.map(function(z) {
      let line = '';
      if (!opts.domain) {
        line += cUtil.strPad(z.domain, ' ', 32) + ' ';
      }
      if (this.args.withId) {
        line += cUtil.strPad('' + z.zoneId, ' ', 6, -1) + ' ';
      }
      line += z.name;
      return line;
    }.bind(this)).join(os.EOL);
  }

  static *show() {
    let self = this;
    if (this.arg && this.args.domain) {
      let resp = yield self.user.client.send('get', '/zone/byName', {name: this.arg, domain: this.args.domain});
      if (null != resp.body.error) {
        throw new Error(resp.body.error.message);
      }
      let zone = resp.body.result;
      return this.args.withIdOnly ? zone.zoneId : cZone.summary(zone);

    } else {

      let zoneIds = [];
      if (!this.arg) {
        let results = cUtil.toLines(yield cUtil.readPipe());
        let isAllNumeric = results.filter(function (r) {
          return /^\d+$/.test(r)
        });
        if (isAllNumeric.length != results.length) {
          throw new Error('Piped results must be zone ids only');
        }
        zoneIds = zoneIds.concat(results);

      } else {
        zoneIds.push(this.arg);
      }

      yield pasync.mapLimit(zoneIds, 20, function(zoneId) {
        return co(function *() {
          let resp = yield self.user.client.showZone(zoneId);
          if (null != resp.error) {
            throw new Error(resp.body.error.message);
          }

          resp = cZone.summary(resp.body.result);
          self.log.info(resp);
          self.log.info(os.EOL);
          return resp;
        });
      });
    }
  }

  static *create() {
    let errors = [];

    if (!this.arg) {
      errors.push('Zone name is required');
    }
    if (!this.args.domain) {
      errors.push('Domain is required');
    }

    if (errors.length) {
      throw new Error(errors.join('. '));
    }

    let name = this.arg;
    let domain = this.args.domain;
    let chefServer = this.args.chefServer;

    let resp = yield this.user.client.createZone(this.arg, domain, chefServer);
    if (null != resp.body.error) {
      throw new Error(resp.body.error);
    }

    return resp.body;
  }

  static *delete() {
    if (!this.arg) {
      throw new Error('Zone id is required');
    }

    let resp = yield this.user.client.deleteZone(this.arg);
    if (null != resp.body.error) {
      throw new Error(resp.body.error);
    }

    return resp.body;
  }
}

module.exports = Zone;